Python Virtualenv Setup
-----------------------

Requirements/dependencies for script:

	- virtualenv must be installed
	- autoenv must be installed for it to work
	- bitbucket-cli should be installed in order to create a bitbucket repo for your project (http://goo.gl/qRnuiF)
	- git should be installed in order to have the repo initialized

This scripts whole purpose is to setup a python virtualenv environment for your project, and to get it ready for you to use.  It takes a bit of the initialization and setup off of your hands so that you can get down to the business of coding.

The script takes a single object, the name of the project environment to create, and creates its project directory, sets it up as a virtualenv with autoenv enabled.  It then prompts you to see if you want to create a repository in either bitbucket or github, and will 'git init' the project directory.  That way all you have to do is create files, add them and commit them.   

The script assumes that you have the following directories already existing:
	
	~/coding/python/projects

You are free to change that if you wish.  The script also sets up for autoenv, which is a sweet utility that makes it so you don't have to activate your environment.  It will do that for you upon entering the main project directory.  You will need to make sure that you have autoenv setup correctly, in order for it to work.  You can find instructions here:  https://github.com/kennethreitz/autoenv

If you have any issues with the script, please feel free to open a ticket in the issues area for this repository.


Installation
------------

The script itself is just that, a script.  Its best if you simply copy it to your ~/bin directory and ensure that your $PATH contains that directory.  Please make sure that you have the requirements/dependencies resolved before running the script.
