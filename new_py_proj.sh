#!/bin/bash
 
###############################################################################
# A script to setup a python virtualenv environment and make it autoenv
# enabled.
# This script also sets up the .env file for autoenv to work, so you need to 
# ensure that you have autoenv installed and configured properly.
###############################################################################


#######################################
############# Functions ###############
#######################################
create_git() {
    echo "What is your github user name?"
    echo "(you will be prompted for your password below)"
    read -p ">>> " gh_username
    curl -u "$gh_username" https://api.github.com/user/repos -d '{"name":"'"${project_name}"'"}'
    echo ""
    echo ""
    echo "Set your origin as follows: git remote add origin git@github.com:${gh_username}/${project_name}.git"
}

create_bb() {
  echo "What is your bitbucket user name?"
  echo "(you will be prompted for your password below)"
  read -p ">>> " bb_username
  bb create --username $bb_username --public --scm git --protocol ssh $project_name
  echo "Set your origin as follows: git remote add origin git@bitbucket.com:${bb_username}/${project_name}.git"
}

create_flask_proj() {
  if [[ -n "$proj_size_resp" ]]
  then
    case $proj_size_resp in
	  "s")
        echo "Creating a small flask project structure."
        echo "All files will be empty and ready for use."
        mkdir -p "${full_project_path}/static" "${full_project_path}/templates"
        touch "${full_project_path}/static/style.css" "${full_project_path}/templates/404.html"
        touch "${full_project_path}/templates/index.html" "${full_project_path}/${project_name}.py"
        echo "Small Project structure created."
		;;
	  "l")
        echo "Creating a large flask project structure."
        echo "All files will be empty and ready for use."
        mkdir -p "${full_project_path}/${project_name}/static" "${full_project_path}/${project_name}/templates"
        touch "${full_project_path}/${project_name}/static/style.css" "${full_project_path}/${project_name}/templates/404.html"
        touch "${full_project_path}/${project_name}/templates/index.html" "${full_project_path}/${project_name}/__init__.py"
        touch "${full_project_path}/${project_name}/views.py" "${full_project_path}/runserver.py"
        echo "Large Project structure created."
		;;
	  "n")
	    echo "Skipping project template, going with base setup....."
		;;
	  *)
        echo "Valid response note entered to project size. Exiting..."
	    exit 
		;;
	esac
  fi
} # End create_flask_proj


# Script starting notes and notifications
echo "Before continuing, you need to make sure that: "
echo "1. virtualenv, virtualenvwrapper and autoenv are installed via pip before continuing"
echo "2. 'source /usr/local/bin/virtualenvwrapper.sh' is in your .bashrc|.profile (whichever you use)"
echo "3. The WORKON_HOME variable is defined in your .bashrc|.profile"
echo "4. 'source /usr/local/bin/activate.sh' is in your .bashrc|.profile"
echo "5. Ensure you edit the project_path and env_path variables in this script"
echo ""
echo "If the above conditions are not met, the results of this script will not be correct."
echo ""


# CHECKS
#

# What am I dealing with, Mac or Linux?
if [[ -e /usr/bin/virtualenvwrapper.sh ]]
then
  vwscript="/usr/bin/virtualenvwrapper.sh"
else
  vwscript="/usr/local/bin/virtualenvwrapper.sh"
fi


# Check if virtualenv, virtualenvwrapper are installed
if [[ `pip list | grep virtualenvwrapper` ]]
then
  echo "virtualenv and virtualenvwrapper are installed..."
  source $vwscript
else
  echo "virtualenv and/or virtualenvwrapper are not installed.  Please install them and rerun."
  exit
fi
    

work_on=`type -t workon`


# Check if autoenv is installed
if [[ `pip list | grep autoenv` ]]
then
  echo "autoenv is installed..."
else
  echo "autoenv is not installed.  Please install it and rerun."
  exit
fi


# Check if git is installed
if [[ `command -v git` ]]
then
  echo "git is installed..."
else
  echo "git is not installed and needs to be.  Please install and rerun."
  exit
fi


# Check if bitbucket-cli is installed
if [[ `pip list | grep bitbucket-cli` ]]
then
  echo "bitbucker-cli is installed..."
else
  echo "bitbucket-cli is not installed.  You will not be able to create a bitbucket project if you do not install it first.  It can be installed with 'pip install bitbucket-cli'"
fi


# Ensure virtualenvwrapper.sh is sourced
if [[ $work_on == "function" ]]
then
  echo "virtualenvwrapper.sh is sourced.  Please ensure it is sourced in your .bashrc|.profile"
else
  echo "virtualenvwrapper.sh doesn't seem to be sourced in your .bashrc|.profile.  Fix this and rerun."
  exit
fi


# Check if project name was provided.  Error and exit if not
if [[ -n $1 ]]
then
  project_name=$1
else
  echo "You didn't specify a project name!"
  echo ""
  echo "Usage:  new_py_proj.sh <project_name>"
  echo ""
  echo "  <project_name> : The name of the project you want created"
  exit
fi


# Script run...

project_path="$HOME/coding/python/projects/"
full_project_path="${project_path}${project_name}"
env_path="${full_project_path}/.env"

### Check if project path directory exists
#if [ -d $project_path ]
#then
#  echo "Projects directory exists... good"
#  echo "Creating project direcory..."
#  mkdir -p $full_project_path
#else
#  echo "Projects directory missing..." 
#  echo "Creating projects directory as well as the project directory..."
#  mkdir -p $project_path
#  echo "created ${project_path}"
#fi
if [ -d ${full_project_path} ]
then
  echo "Project directory exists... continuing"
else
  mkdir -p ${full_project_path}
  echo "Project directory created..."
fi

# Time to create the project template
echo "Will this be a small or large flask project?"
echo "If you do not know the difference, please read this:"
echo "http://flask.pocoo.org/docs/0.10/patterns/packages/"
echo " "
echo "Please enter 's' for small, 'l' for large or 'n' for no project template:"
read -p "s/l/n ? " proj_size_resp
create_flask_proj

# Create virtual environment and populate the .env file
echo "Creating virtual env....."
cd $full_project_path
mkvirtualenv $project_name
echo "Setting up autoenv dotfile..."
echo "echo '$project_name environment ACTIVE'" >> $env_path
echo "workon $project_name" >> $env_path

# See if the user wants to setup a bitbucket repository
# If so, prompt for username, create it and then 'git init'
# the project directory
echo ""
echo "Do you want to create a bitbucket repository for this project?"
echo "NOTE: Empty response is assumed to be a y/yes"
read -p "y/n? " bb_ques_resp
if [[ -z $bb_ques_resp || "$bb_ques_resp" == *"y"* ]]
then
  create_bb
else
  echo "Skipping bitbucket setup....."
  echo "Do you want to create a github repository for this project?"
  echo "NOTE: Empty response is assumed to be a y/yes"
  read -p "y/n? " gh_ques_resp
  if [[ -z $gh_ques_resp || "$gh_ques_resp" == *"y"* ]]
  then
    create_git
  else
    echo "Skipping github setup....."
  fi
fi

echo "Initializing local repository....."
git init $full_project_path
echo "Do not forget to deacvitate the environment after you work on it"
echo "Setup completed!"



